import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.options import options
from urls import url_patterns

class SamhsaApp(tornado.web.Application):
    def __init__(self):
        tornado.web.Application.__init__(self, url_patterns)


def main():
    app = SamhsaApp()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(5000)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()