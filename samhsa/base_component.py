from abc import ABC, abstractmethod

class BasePipelineComponent(ABC):
    """Abstract Base class for use in the processing pipeline. Each component
    provides functionality that will enrich the processing pipeline with
    data that will be returned to the client. For example a Spellcheck Compoenent
    will provide the processing pipeline with the corrected version of the query
    from the client.
    """

    required_input_keys = []

    def process(self, input_dict):
        """Method that takes data dictionary as input, and provides
           data to be added to the dictionary.

        Arguments:
            input_dict {dict} -- Data availale to this component.

        Returns:
            dict -- Returns the data that should be added to the processing pipeline.
        """

        input_dict_subset = {k:input_dict[k] for k in self.required_input_keys}
        value_dict = self._process(input_dict_subset)
        return value_dict

    @abstractmethod
    def _process(self, input_dict):
        """Method that will do the actual logic of the component. e.g. A spellchecker
        will checkdo spelling here.

        Decorators:
            abstractmethod

        Arguments:
            input_dict {dict} -- Data available to this component during processing.

        Returns dict -- Key value pairs to be added to the pipeline data.s
        """
        pass
