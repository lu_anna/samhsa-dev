from samhsa.handlers.base import BaseHandler
from samhsa.pipeline import Pipeline
from samhsa.components.spell_check import EnchantComponent
from samhsa.components.entity_extractor import EntityNltkComponent
import json
import logging
logger = logging.getLogger('samhsa.' + __name__)


class PipelineHandler(BaseHandler):
    def initialize(self):
        components = [EnchantComponent(), EntityNltkComponent()] 
        self._pipeline = Pipeline(components)

    def get(self): 
        query = self.get_argument("query")
        result = self._pipeline.run_pipeline(query)
        self.write(json.dumps(result))
