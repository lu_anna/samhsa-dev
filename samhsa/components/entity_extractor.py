from nltk import ngrams
import os
import json
import pandas as pd
from samhsa.base_component import BasePipelineComponent
from nltk.stem.porter import PorterStemmer

porter_stemmer = PorterStemmer()
current_dir = os.path.dirname(os.path.abspath(__file__))
sym_file = os.path.join('samhsa/media/Updated Syptoms Synonyms Map.xlsx')
negation_words = ["not", "can't", "isn't", "no"]


class EntityNltkComponent(BasePipelineComponent):

    required_input_keys = ["query"]

    def _process(self, input_dict):
        query = input_dict["query"]
        keywords, mapping_list = get_all_symptoms(query)
        return {"entities": mapping_list}

def load_symptoms():
    df = pd.read_excel(sym_file, 1, header=1)


    symptom_map = {}
    symptom_list = set()
    antonym_list = set()
    antonym_map = {}
    synonyms_normalized = {}
    antonyms_normalized = {}

    for index, row in df.iterrows():

        symptom_name = row["Symptom Name"]
        if isinstance(symptom_name, str):
            symptom_name = symptom_name.strip()
            symptom_name = " ".join([porter_stemmer.stem(s.strip()) for s in symptom_name.split()])
            symptom_list.add(symptom_name.strip())
            try:
                mapping = symptom_map[symptom_name]
            except:
                symptom_map[symptom_name] = []
                mapping = symptom_map.get(symptom_name)
            if row["Symptom Name"] not in mapping:
                mapping.append(row["Symptom Name"].strip())



        clarifyying_question = row["Clarifying Questions "]
        if isinstance(clarifyying_question, str):
            clarifyying_question = row['Clarifying Questions '].split(",")
        else:
            clarifyying_question = []
        for q in clarifyying_question:

            original = q.strip().lower()
            q = q.strip().lower()
            q = " ".join([porter_stemmer.stem(s.strip()) for s in q.split()])
            symptom_list.add(q.strip())

            try:
                row["Symptom Name"]
                mapping = symptom_map[q]
            except:
                symptom_map[q] = []
                synonyms_normalized[original] = []
                mapping = symptom_map.get(q)
            if row["Symptom Name"] not in mapping:
                mapping.append(row["Symptom Name"].strip())



        synonyms = row['Synonyms']
        if isinstance(synonyms, str):
            synonyms = row['Synonyms'].split(",")
        else:
            synonyms = []
        for synonym in synonyms:
            original = synonym.strip()
            synonym = synonym.strip().lower()
            synonym = " ".join([porter_stemmer.stem(s.strip()) for s in synonym.split()])
            symptom_list.add(synonym.strip())
            try:
                row["Symptom Name"]

                mapping = symptom_map[synonym]
            except:

                symptom_map[synonym] = []
                synonyms_normalized[original] = []
                mapping = symptom_map.get(synonym)
            if row["Symptom Name"] not in mapping:
                mapping.append(row["Symptom Name"].strip())

            if original == "exhausting":
                pass

        antonyms = row['Antonyms']
        if isinstance(antonyms, str):
            antonyms = antonyms.split(",")
        else:
            antonyms = []
        for antonym in antonyms:
            original = antonym
            antonym = antonym.strip().lower()
            antonym = " ".join([porter_stemmer.stem(s.strip()) for s in antonym.split()])
            antonym_list.add(antonym)
            try:
                mapping = antonym_map[antonym]
                #normalized_mapping = symptom_map[synonym]
            except:
                antonym_map[antonym] = []
                mapping = antonym_map.get(antonym)
            if row["Symptom Name"] not in mapping:
                mapping.append(row["Symptom Name"].strip())
    return symptom_list, symptom_map, antonym_list, antonym_map, synonyms_normalized, antonyms_normalized

symptom_list, symptom_map, antonym_list, antonym_map, synonyms_normalized, antonyms_normalized  = load_symptoms()


def get_symptoms(keywords):
    symptoms = []
    for keyword in keywords:
        try:
            symptom  = symptom_map[keyword]
            for s in symptom:
                symptoms.append(s)
        except:
            pass
    return symptoms

def get_antonyms(antonyms):
    a_list = []
    for keyword in antonyms:
        try:
            symptom  = antonym_map[keyword.strip()]
            for a in symptom:
                a_list.append(a)
        except:
            pass
    return a_list

def get_ngrams(numgrams, text):
    grams = []
    numgrams = ngrams(text.split(), numgrams)
    for gram in numgrams:
        grams.append(" ".join(gram).lower())
    return grams

def get_all_symptoms(query):
    keywords, antonyms, unchanged_keywords = get_keywords(query)
    symptoms = get_symptoms(keywords)
    antonyms = get_antonyms(antonyms)
    symptoms = symptoms + antonyms
    return  list(set(symptoms)), unchanged_keywords

def get_keywords(text):
    symptoms = []
    added_index_array = []
    original_keywords = {}
    antonyms = []
    text_array = text.split() if  text else []

    for times in range(6,0,-1):
        ngrams_two = get_ngrams( times, text)
        append_synonyms(symptoms, antonyms, ngrams_two, added_index_array, times, original_keywords, text_array)


    return set(symptoms), set(antonyms), original_keywords

def append_synonyms(symptoms, antonyms, ngrams_list, added_index_array, num_ngrams, unchanged_keywords, text_array):
    for idx, gram in enumerate(ngrams_list):
        is_antonym, negated_index = isAntonym(idx, text_array)
        if not is_antonym:
            original = gram
            gram = " ".join([porter_stemmer.stem(s.strip()) for s in gram.split()])
            if gram in symptom_list and not_added(idx, num_ngrams, added_index_array):
                symptoms.append(gram)
                unchanged_keywords[original] = symptom_map[gram]
                lower_bound = idx
                for r in range(lower_bound, lower_bound + num_ngrams):
                    added_index_array.append(r)
        else:
            original = gram
            gram = " ".join([porter_stemmer.stem(s.strip()) for s in gram.split()])
            if gram in antonym_list and not_added(idx, num_ngrams, added_index_array):
                antonyms.append(gram)
                antonym_text = " ".join(text_array[negated_index:idx + num_ngrams])
                unchanged_keywords[antonym_text] = antonym_map[gram]
                lower_bound = idx
                for r in range(negated_index, lower_bound + num_ngrams):
                    added_index_array.append(r)

def not_added(idx, num_ngrams, added_index_array):

    for r in range(idx, idx + num_ngrams):
        if r in added_index_array:
            return False
    return True

def isAntonym(idx, text_array):
    previous = idx - 1
    twoPrevious = idx - 2

    previousWord = text_array[previous] if previous >= 0 else ""
    twoPreviousWord = text_array[twoPrevious] if twoPrevious >= 0 else ""

    index_of_negation = -1
    is_negated = False

    if previousWord in negation_words:
        index_of_negation = previous
        is_negated = True
    elif twoPreviousWord in negation_words:
        index_of_negation = twoPrevious
        is_negated = True

    return is_negated, index_of_negation