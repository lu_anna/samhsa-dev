from samhsa.handlers.pipeline import PipelineHandler

url_patterns = [
    (r"/pipeline", PipelineHandler)
]